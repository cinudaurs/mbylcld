/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.magnum.dataup;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.magnum.dataup.VideoSvcApi;
import org.magnum.dataup.model.Video;
import org.magnum.dataup.model.VideoStatus;
import org.magnum.dataup.model.VideoStatus.VideoState;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class VideoSvcController {

	/**
	 * You will need to create one or more Spring controllers to fulfill the
	 * requirements of the assignment. If you use this file, please rename it
	 * to something other than "AnEmptyController"
	 * 
	 */ 
	//private Collection<Video> videos = new CopyOnWriteArrayList<Video>();
	private static final AtomicLong currentId = new AtomicLong(0L);
	private Map<Long, Video> videoMap = new HashMap<Long, Video>();
	private VideoFileManager videoDataMgr; 	
	
	
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH, method=RequestMethod.POST)
	public @ResponseBody Video addVideo(@RequestBody Video v){
		
		
		v = save(v);
		v.setDataUrl(getDataUrl(v.getId()));
		
		return v;
		
		
	}
	
	   public Video save(Video entity) {

           checkAndSetId(entity);

           videoMap.put(entity.getId(), entity);

           return entity;

}
	
	
	   private void checkAndSetId(Video entity) {

           if(entity.getId() == 0){

                       entity.setId(currentId.incrementAndGet());

           }

}


	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH, method=RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList(){
		
		return videoMap.values();
		
	}
	
	private String getDataUrl(long videoId){
        String url = getUrlBaseForLocalServer() + "/video/" + videoId + "/data";
        return url;
    }

 	private String getUrlBaseForLocalServer() {
	   
 		HttpServletRequest request = 
	       ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	   String base = 
	      "http://"+request.getServerName() 
	      + ((request.getServerPort() != 80) ? ":"+request.getServerPort() : "");
	   return base;
	}
	
	
 	
 	@RequestMapping(value="/video/{id}/data", method=RequestMethod.POST)
    public @ResponseBody VideoStatus setVideoData(@PathVariable("id") long id, @RequestParam("data") MultipartFile videoData, HttpServletResponse response) {
		
 		Video v = videoMap.get(id);
 		
 		VideoStatus resp = null;
 		
 		if (v == null){
 			
 			response.setStatus(404);
 			
 		}
 		
 		
 		if(v != null){	
 			
 			try {
 								
				saveSomeVideo(v, videoData);
				response.setStatus(200);
				resp =  new VideoStatus(VideoState.READY);
				
 				} catch (IOException e) {
 					e.printStackTrace();
			}			
 	
 		}
		return resp;	
    }

 	 public void saveSomeVideo(Video v, MultipartFile videoData) throws IOException {

 		 videoDataMgr = VideoFileManager.get();
 		 videoDataMgr.saveVideoData(v, videoData.getInputStream());

    }
 	 
 	public void serveSomeVideo(Video v, HttpServletResponse response) throws IOException {
 	     
 		 videoDataMgr = VideoFileManager.get();
 	     videoDataMgr.copyVideoData(v, response.getOutputStream());
 	}
 	 
 	 
 	 
 	 @RequestMapping(value= "/video/{id}/data", method=RequestMethod.GET )
 	 public void getData(@PathVariable("id") long id, HttpServletResponse response) throws IOException {
		
 		Video v = videoMap.get(id);
 	 		
 		if(v != null){
 		
 		try {
			serveSomeVideo(v, response);

			response.setStatus(200);
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
 	
 	 }else{
 		 
 		if ( v == null){
 			response.setStatus(404);
 		}
 		
 		 
 	 }



 	 }
	
	
	
}

/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.magnum.mobilecloud.video;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.repository.Video;
import org.magnum.mobilecloud.video.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;

@Controller
public class OAuthController {
	
	
		@Autowired
		private VideoRepository videos;
	
	
	/* GET /video
		- Returns the list of videos that have been added to the
		  server as JSON. The list of videos should be persisted
		  using Spring Data. The list of Video objects should be able 
		  to be unmarshalled by the client into a Collection<Video>.
		- The return content-type should be application/json, which
		  will be the default if you use @ResponseBody
		  */
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH, method=RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList(){
		return Lists.newArrayList(videos.findAll());
	}
	
/*	POST /video
	   - The video metadata is provided as an application/json request
	     body. The JSON should generate a valid instance of the 
	     Video class when deserialized by Spring's default 
	     Jackson library.
	   - Returns the JSON representation of the Video object that
	     was stored along with any updates to that object made by the server. 
	   - **_The server should store the Video in a Spring Data JPA repository.
	   	 If done properly, the repository should handle generating ID's._** 
	   - A video should not have any likes when it is initially created.
	   - You will need to add one or more annotations to the Video object
	     in order for it to be persisted with JPA.
	     */
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH, method=RequestMethod.POST)
	public @ResponseBody Video addVideo(@RequestBody Video v){
		 
		return videos.save(v);
		 
	}

	
	
	/*
	 * - Returns the video with the given id or 404 if the video is not found.
	 */
	
	@RequestMapping(value= "/video/{id}", method=RequestMethod.GET )
	 public @ResponseBody Video getVideo(@PathVariable("id") long id, HttpServletResponse response){
		
		Video v = null;
		
		v = videos.findOne(id);
		          	 		
		if(v != null){
		
		response.setStatus(200);
	
	 }else{
		 
		if ( v == null){
			
			response.setStatus(404);
		}
	 }
		return v;
	}
	
	
	
	
	/*
	 *	- Allows a user to like a video. Returns 200 Ok on success, 404 if the
     		video is not found, or 400 if the user has already liked the video.
   		- The service should should keep track of which users have liked a video and
     		prevent a user from liking a video twice. A POJO Video object is provided for 
     		you and you will need to annotate and/or add to it in order to make it persistable.
   		- A user is only allowed to like a video once. If a user tries to like a video
      		a second time, the operation should fail and return 400 Bad Request.
	 * 
	 */
	@RequestMapping(value="/video/{id}/like", method=RequestMethod.POST)
    public @ResponseBody void like(@PathVariable("id") long id, Principal p, HttpServletResponse response){
     
		Video v = null;
		boolean likeSuccess = false;
	
		v = getVideo(id, response);
		
		if (v != null){
			
			likeSuccess = v.likeVideo(p.getName());
			
			if(likeSuccess)
			{
				videos.save(v);
				response.setStatus(200);				
			}else{
				response.setStatus(400);
			}

		}else{
			
			response.setStatus(404);
			
						
		}
    }
	
	@RequestMapping(value = "/video/{id}/unlike", method = RequestMethod.POST)
	public void unLike(@PathVariable("id") long id, Principal p,
			HttpServletResponse response) {
		
		Video v = null;
		boolean unLikeSuccess = false;
		
		v = getVideo(id, response);

		if (v != null) {
			
			unLikeSuccess = v.unlikeVideo(p.getName());
			
			if (unLikeSuccess) {
				videos.save(v);
				response.setStatus(200);
			} else {
				response.setStatus(400);
			}
		}
		else{
			
			response.setStatus(404);
			
		}
	}
	
	@RequestMapping(value = "/video/{id}/likedby", method = RequestMethod.GET)
	public @ResponseBody
	List<String> likedBy(@PathVariable("id") long id, HttpServletResponse response) {
		
		Video v = getVideo(id, response);
		
		List<String> listOfLikes = null;

		if(v == null)
		{
			response.setStatus(404);
		}
		
				
		if (v != null) {
			
			listOfLikes = v.getListOfLikes();
			response.setStatus(200);
			
		}
		
		return listOfLikes;

		
	}
	
	@RequestMapping(value="/video/search/findByName", method=RequestMethod.GET)
	public @ResponseBody List<Video> findByName(@RequestParam("title") String title){
		
		List<Video> listOfVideos = null;
		
		listOfVideos = (List<Video>) videos.findByName(title);
		
		if(listOfVideos == null)
			return new ArrayList<Video>();
		
		return listOfVideos;
		
	}
	
	@RequestMapping(value="/video/search/findByDurationLessThan", method=RequestMethod.GET)
	public @ResponseBody List<Video> findByDurationLessThan(@RequestParam("duration") long duration){
		
		List<Video> listOfVideos = null;
		listOfVideos = (List<Video>) videos.findByDurationLessThan(duration);
		
		if(listOfVideos == null)
			return new ArrayList<Video>();
		
		return listOfVideos;
				
		
	}
 	
}
